import { ChangeDetectorRef, Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from './sidenav-service.service';
import * as countryList from 'country-list';
import * as go from 'gojs';
import Swal from 'sweetalert2';
import { DataSyncService, DiagramComponent } from 'gojs-angular';
import { saveAs } from 'file-saver';
import { NodeService } from "./node-service.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {
  title = 'Flight Manager';
  countries = Object.entries(countryList.getNameList());

  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(private cdr: ChangeDetectorRef, private sidenavService: SidenavService, private nodeService: NodeService) { }

  ngOnInit(): void {
    this.nodeService.currentNode.subscribe(node => { 
      if (Object.keys(node).length) {
        this.diagramNodeData.push(node)
      }
    });
  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }

  // initialize diagram / templates
  public initDiagram(): go.Diagram {

    const $ = go.GraphObject.make;
    const dia = $(go.Diagram, {
      'undoManager.isEnabled': true,
      model: $(go.GraphLinksModel,
        {
          linkToPortIdProperty: 'toPort',
          linkFromPortIdProperty: 'fromPort',
          linkKeyProperty: 'key' // IMPORTANT! must be defined for merges and data sync when using GraphLinksModel
        }
      )
    });
    
    const makePort = function(id: string, spot: go.Spot) {
      return $(go.Shape, 'Circle',
        {
          opacity: .5,
          fill: 'gray', strokeWidth: 0, desiredSize: new go.Size(8, 8),
          portId: id, alignment: spot,
          fromLinkable: true, toLinkable: true
        }
      );
    } 
    
    function makeImagePath(icon) { return "/assets/flags/" + icon; }

    // define the Node template
    dia.nodeTemplate =
      $(go.Node, 'Spot',
        { isClipping: true, scale: 2},
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Circle", { width: 32, strokeWidth: 0} ),
        $(go.Picture,
          { width: 32, height: 32 }, 
          new go.Binding("source", "icon", makeImagePath)),  
          {
            toolTip:  // define a tooltip for each node that displays the color as text
              $("ToolTip",
                $(go.TextBlock, { margin: 4 },
                  new go.Binding("text", "key"))
              )  // end of Adornment
          },
      // Ports
      makePort('r', go.Spot.Right),
      makePort('l', go.Spot.Left),
    );

    dia.linkTemplate =
    $(go.Link,
      $(go.Shape, { strokeWidth: 3.5 }),
      $(go.Shape, { toArrow: "Standard" }),  // this is an arrowhead
      $(go.TextBlock,                        // this is a Link label
        new go.Binding("text", "connection").makeTwoWay(), 
        { segmentOffset: new go.Point(0, -10) })
    );

    dia.addDiagramListener("ObjectDoubleClicked",
      function(e) {
        let part = e.subject.part;
        if (part instanceof go.Link) {
          getNewConenction(part);
        }
      });

    dia.addDiagramListener("LinkDrawn", function(e) { 
      let link = e.subject;
      getNewConenction(link);
    });

    function getNewConenction(part) {
      Swal.fire({
        title: 'Connection type',
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Set Connection',
        allowOutsideClick: true,
      }).then((result) => {
        if (result.value) 
          dia.model.setDataProperty(part.data, "connection", result.value);
      })
    }

    return dia;
  }

  public diagramNodeData: Array<go.ObjectData> = [
    { key: 'andorra', icon: "ad.png", loc: "-300 0"},
    { key: 'argentina', icon: "ar.png", loc: "-100 0"},
    { key: 'brazil', icon: "br.png", loc: "100 0"},
    { key: 'turkey', icon: "tr.png", loc: "300 0"},
  ];
  
  public diagramLinkData: Array<go.ObjectData> = [
    { from: 'andorra', to: 'argentina', connection: "flight", fromPort: 'r', toPort: 'l', key: -1 },
    { from: "brazil", to:"turkey", connection: "car", fromPort:"r", toPort:"l", key: -2 },
    { from: "argentina", to:"brazil", connection: "train", fromPort:"r", toPort:"l", key: -3 }
  ];

  public diagramDivClassName: string = 'DiagramDiv';
  public diagramModelData = { prop: 'value' };

  // When the diagram model changes, update app data to reflect those changes
  public diagramModelChange = function(changes: go.IncrementalData) {
    this.diagramNodeData = DataSyncService.syncNodeData(changes, this.diagramNodeData);
    this.diagramLinkData = DataSyncService.syncLinkData(changes, this.diagramLinkData);
    this.diagramModelData = DataSyncService.syncModelData(changes, this.diagramModelData);
  };

  public saveJson(): void {
    const $ = go.GraphObject.make;
    const diagram = $(go.Diagram);
    diagram.model = new go.GraphLinksModel(this.diagramNodeData, this.diagramLinkData);
    const blob = new Blob([diagram.model.toJson()], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "Flights.json")
  }

  public loadJson(text) {
    let json = JSON.parse(text);
    this.diagramNodeData = json["nodeDataArray"];
    this.diagramLinkData = json["linkDataArray"];
  }
   
  public clearGraph() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.diagramNodeData = [];
        this.diagramLinkData = [];
        Swal.fire(
          'Deleted!',
          'Your graph has been deleted.',
          'success'
        )
      }
    })
  }
  
  public readFile(event): void { 
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.onload = (e) => {
      let text = reader.result;
      this.loadJson(text);
    }
    reader.readAsText(file);
  }    
}
