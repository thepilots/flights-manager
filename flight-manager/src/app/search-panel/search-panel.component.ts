import { Component, OnInit } from '@angular/core';
import * as countryList from 'country-list';
import { NodeService } from '../node-service.service';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.scss']
})
export class SearchPanelComponent implements OnInit {
  countries = Object.entries(countryList.getNameList());
  searchedCountries = this.countries;
  searchExpression: string = "";
  newCountry: {};

  constructor(private nodeService: NodeService) {}

  onSearchChange(event): void {
    if (this.searchExpression == "") {
      this.searchedCountries = this.countries;
      return;
    }
    let expression = this.searchExpression.toLowerCase();
    this.searchedCountries = this.countries.filter(country => country[0].startsWith(expression));
  }

  ngOnInit() {
    this.nodeService.currentNode.subscribe(node => this.newCountry = node);
  }

  onCountryClick(index) {
    this.nodeService.changeNode({key: this.searchedCountries[index][0], icon: this.searchedCountries[index][1].toString().toLowerCase() + ".png"});  
  }
}
