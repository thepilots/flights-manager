import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../../sidenav-service.service';

@Component({
  selector: 'app-panel-header',
  templateUrl: './panel-header.component.html',
  styleUrls: ['./panel-header.component.scss']
})
export class PanelHeaderComponent implements OnInit {

  constructor(private sidenav: SidenavService) { }

  closeSidenavBar() {
    this.sidenav.close();
  }

  ngOnInit() {
  }

}
