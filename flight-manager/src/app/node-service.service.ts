import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class NodeService {

  private nodeSource = new BehaviorSubject({});
  currentNode = this.nodeSource.asObservable();

  constructor() { }

  changeNode(newNode: {}) {
    this.nodeSource.next(newNode);
  }
}